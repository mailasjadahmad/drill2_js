const inventory = require("./inventory");

function carModelSort(inventory) {
  if (Array.isArray(inventory) && inventory.length > 0) {
    const allObjects = inventory.every((item) => typeof item === "object");
    if (allObjects) {
      inventory.sort((car1, car2) => {
        const model1 = car1.car_model;
        const model2 = car2.car_model;

        if (typeof model1 === "string" && typeof model2 === "string") {
          return model1.localeCompare(model2);
        } else {
          return model1 - model2;
        }
      });

      const sortedArray = inventory.map((singleCar) => singleCar.car_model);

      return sortedArray;
    } else {
      console.warning("all the element in the array is not an object");
    }
  } else {
    console.warn("please enter a valid array ");
  }
}

module.exports = carModelSort;
