const inventory = require("./inventory");

function carInfoOfAudiBmw(inventory) {
  if (Array.isArray(inventory) && inventory.length > 0) {
    const allObjects = inventory.every((item) => typeof item === "object");
    if (allObjects) {
      const carInfoOfAudiBmwArr = inventory
        .map((singleCar) =>
          singleCar.car_make == "Audi" || singleCar.car_make == "BMW"
            ? singleCar
            : null
        )
        .filter((singleCar) => singleCar !== null);

      return carInfoOfAudiBmwArr;
    } else {
      console.warning("all the element in the array is not an object");
    }
  } else {
    console.warn("please enter a valid array ");
  }
}

module.exports = carInfoOfAudiBmw;
