const inventory = require("./inventory");

function carYears(inventory) {
  if (Array.isArray(inventory) && inventory.length > 0) {
    const allObjects = inventory.every((item) => typeof item === "object");
    if (allObjects) {
      const carYearsArr = inventory.map((singleCar) => singleCar.car_year);

      return carYearsArr;
    } else {
      console.warning("all the element in the array is not an object");
    }
  } else {
    console.warn("please enter a valid array ");
  }
}

module.exports = carYears;
