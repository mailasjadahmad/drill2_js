const inventory = require("./inventory");

function carInfo(inventory, carId) {
  if (
    Array.isArray(inventory) &&
    inventory.length > 0 &&
    typeof carId == "number"
  ) {
    const allObjects = inventory.every((item) => typeof item === "object");
    if (allObjects) {
      let carInfoObject = inventory.filter(
        (singleCar) => singleCar.id == carId
      );
      const result = `Car ${carInfoObject[0].id} is a car of year ${carInfoObject[0].car_year} and model ${carInfoObject[0].car_model}`;
      return result;
    } else {
      console.warning("all the element in the array is not an object");
    }
  } else {
    console.warn("please enter a valid array ");
    console.log(inventory);
  }
}

module.exports = carInfo;
