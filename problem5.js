const inventory = require("./inventory");
let carYears = require("./problem4");

function carsOlderThan(inventory, carYears, year) {
  if (
    Array.isArray(inventory) &&
    inventory.length > 0 &&
    typeof year == "number"
  ) {
    const allObjects = inventory.every((item) => typeof item === "object");
    if (allObjects) {
      let tempCars = [];
      let tempIndex = 0;

      tempCars = carYears.map((singleCar, index) =>
        singleCar < year ? inventory[index] : null
      );

      const carsOlderThanArr = tempCars.filter(
        (singleCar) => singleCar !== null
      );
      return carsOlderThanArr;
    } else {
      console.warning("all the element in the array is not an object");
    }
  } else {
    console.warn("please enter a valid array and year ");
  }
}
module.exports = carsOlderThan;
