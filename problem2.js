const inventory = require("./inventory");

function lastCarInfo(inventory) {
  if (Array.isArray(inventory) && inventory.length > 0) {
    const allObjects = inventory.every((item) => typeof item === "object");
    if (allObjects) {
      const lastCarInfoObject = inventory.filter(
        (singleCar) => singleCar.id == inventory.length
      );

      const result = `Last car is a ${lastCarInfoObject[0].car_make} and model ${lastCarInfoObject[0].car_model}`;
      return result;
    } else {
      console.warning("all the element in the array is not an object");
    }
  } else {
    console.warn("please enter a valid array ");
  }
}

module.exports = lastCarInfo;
